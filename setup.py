"""Flywheel-apps pre-commit hooks"""
import os

from setuptools import setup, find_packages

INSTALL_REQUIRES = [
    "flywheel-gear-toolkit",
]

DEV_REQUIRES = []

VERSION = os.getenv("CI_COMMIT_TAG", "0.0.0.dev1")

setup(
    name="fw-gear-pre-commit-hooks",
    version=VERSION,
    description="Flywheel-app pre-commit hooks",
    author_email="support@flywheel.io",
    url="https://gitlab.com/flywheel-io/flywheel-apps/utils/pre-commit-hooks",
    keywords=["Flywheel", "gears", "pre-commit"],
    install_requires=INSTALL_REQUIRES,
    extras_require={"dev": DEV_REQUIRES},
    packages=find_packages(),
    include_package_data=True,
    package_data={},
    license="MIT",
    project_urls={"Source": "https://gitlab.com/flywheel-io/flywheel-apps/utils/pre-commit-hooks"},
    zip_safe=False,
    long_description="""""",
    entry_points={
        "console_scripts": "validate-manifest=fw_gear_pre_commit_hooks.hooks:validate_manifest"
    }
)
