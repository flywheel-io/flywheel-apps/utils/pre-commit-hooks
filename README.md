# Pre-commit hooks for Flywheel Gears

## Installing

Pre-commit can be installed using `pip install pre-commit` or using whatever python
package manager you use (See [pre-commit docs](https://pre-commit.com/#install) for more
info).

## Using these hooks

In your repo that you want to run these hooks on, add the following
to `.pre-commit-config.yaml` in your projects root folder:

```yml
repos:
  - repo: https://gitlab.com/flywheel-io/flywheel-apps/utils/pre-commit-hooks
    rev: master
    hooks:
      - id: isort
      - id: black
      - id: pytest
```

Then run `pre-commit install`.

After you've installed these hooks, everytime you `git commit` they will be run. You can
skip sections of pre-commit by setting the `SKIP` environmental variable before
committing: `SKIP=isort,black git commit`.

Additionally, you can skip all pre-commit hooks by adding the `--no-verify` flag to your
commit: `git commit --no-verify`.

## Description of hooks defined here:

### Poetry

All hooks will be ran using poetry. There must be either an existing
poetry environment, or the poetry executable must be installed and there must be a
pyproject.toml file in the current directory.

Each hook can be configured in the pyproject.toml file.

### Hooks

#### isort

Runs isort to sort python imports

#### black

Formats python documents using black

#### pytest

Runs pytest. By default, pytest hook will require > 90% coverage.
This threshold can be adjusted by setting the `COVER` env var to
the coverage value required in a `.env` file in the repository.

#### pylint

Runs pylint

#### pycodestyle

Runs pycodestyle

#### pydocstyle

Runs pydocstyle
